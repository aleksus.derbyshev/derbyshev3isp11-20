﻿using StroyMagaz_Derbyshev3ISP11_20.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StroyMagaz_Derbyshev3ISP11_20.ClassHelper
{
    internal class ProductListCart
    {
        public static List<Product> ProductCart { get; set; } = new List<Product>();
    }
}
