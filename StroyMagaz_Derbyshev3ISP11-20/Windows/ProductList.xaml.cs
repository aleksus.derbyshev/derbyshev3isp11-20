﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using StroyMagaz_Derbyshev3ISP11_20.ClassHelper;

namespace StroyMagaz_Derbyshev3ISP11_20.Windows
{
    /// <summary>
    /// Логика взаимодействия для PrdoductList.xaml
    /// </summary>
    public partial class ProductList : Window
    {
        public ProductList()
        {
            InitializeComponent();
            GetListService();
        }
        

        private void GetListService()
        {
            LvProductList.ItemsSource = ClassHelper.EF.Context.Product.ToList();
        }
        private void BtnAddToCart_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.Product; // получаем выбранную запись


            ProductListCart.ProductCart.Add(product);

            MessageBox.Show($"Товар {product.Title} добавлен в корзину!");
        }
        private void BtnGoToCart_Click(object sender, RoutedEventArgs e)
        {
            Cart cartWindow = new Cart();
            cartWindow.Show();
            this.Close();
        }
    }
}
    

