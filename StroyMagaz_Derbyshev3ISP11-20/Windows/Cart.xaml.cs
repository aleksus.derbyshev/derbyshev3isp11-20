﻿using StroyMagaz_Derbyshev3ISP11_20.ClassHelper;
using StroyMagaz_Derbyshev3ISP11_20.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StroyMagaz_Derbyshev3ISP11_20.Windows
{
    /// <summary>
    /// Логика взаимодействия для Cart.xaml
    /// </summary>
    public partial class Cart : Window
    {
        public Cart()
        {
            InitializeComponent();
            GetListServise();
        }
        private void GetListServise()
        {
            ObservableCollection<DB.Product> listCart = new ObservableCollection<DB.Product>(ClassHelper.ProductListCart.ProductCart);
            LvProductList.ItemsSource = listCart;
        }
        private void BtnRomoveToCart_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var service = button.DataContext as DB.Product; // получаем выбранную запись

            ClassHelper.ProductListCart.ProductCart.Remove(service);

            GetListServise();
        }
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            ProductList productWindow = new ProductList();
            productWindow.Show();
            this.Close();
        }
        private void BtnPay_Click(object sender, RoutedEventArgs e)
        {
            // покупка

            DB.Order order = new DB.Order();
            order.EmployeID = 1;
            order.ClientID = 5;
            order.OrderDate = DateTime.Now;
            EF.Context.Order.Add(order);
            EF.Context.SaveChanges();
               

            

            

            foreach (var item in ClassHelper.ProductListCart.ProductCart.Distinct())
            {
                ProductOrder productOrder = new ProductOrder();
                productOrder.OrderID = order.ID;
                productOrder.ProductID = item.ID;
                productOrder.Quantity = 1;

                EF.Context.ProductOrder.Add(productOrder);
                EF.Context.SaveChanges();
               
            }
            MessageBox.Show("Заказ успешно оформлен");

            

            GetListServise();

        }
    }
}
        

  

